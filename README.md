# NFDI AAI

- This Documentation is licenced under the Creative Commons CC-BY-SA-3.0 licence.

- Contributions are welcome via pull-requests to https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-aai-doc

- Handbook is published to https://doc.nfdi-aai.de

