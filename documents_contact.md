![logo](images/base4NFDI_kurz_rgb.png){: style="width:200px"}

# Documents

We publish the material of our workshops on this [google
drive](https://drive.google.com/drive/folders/1nT0YpZfOZ9YxQ-qgsA16zZw5KJ0TnTgV).


# Conact

The IAM4NFDI project ca be reached via its mailinglist
[aai-kernteam@lists.kit.edu](mailto:aai-kernteam@lists.kit.edu). Feel free
to contact us.

