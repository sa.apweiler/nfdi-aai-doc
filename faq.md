![logo](images/base4NFDI_kurz_rgb.png){: style="width:200px"}

# Frequently Asked Questions

## Communiy AAIs

> Für die Community AAI werden in der Architekturbeschreibung 4
> verschiedene Software Pakete vorgeschlagen. Es bleibt die Frage, wer
> letztendlich die Community AAI aufsetzt, konfiguriert und betreibt?

Im Rahmen des ([gerade
beantragten](documents/iam4nfdi_initialization.pdf)) B4N/IAM Projekts ist
vorgesehen, dass die vier Pakete im Rahmen des IAM Projekts betrieben
werden.

> Sobald die Betreiberfrage geklärt ist, stellen sich weitere Fragen
> dazu, wie beispielsweise
>
> - ob eine Hochverfügbarkeit Voraussetzung ist
> - SLAs / Backup und Notfallplan
> - Datenschutzfrage
> - Finanzierung sowieso

Am Aufbau von HA, SLAs, etc. pp. (also allen angesprochenen
Punkten) wird im Rahmen des Projekts dann auch zügig gearbeitet.

Damit nicht all zu lange gewartet werden muss, soll es Demo-Instanzen
geben, damit Ihr (die NFDI Konsortien) sich schonmal eine
Entscheidungsgrundlage für die ein- oder andere Lösung bilden könnt.

> Die vier Community AAIs die verwendet werden sollen, existieren bereits,
> richtig? Sie werden nicht als separate Instanz nochmal eingerichtet?

Das hängt von jedem Instanz-Betreiber einzeln ab. Die Software existiert
und wird von den Experten betrieben. Über die Instanzen hierzu wurde noch
nichts festgelegt.

> Jede der vier Community AAIs wird zu einem Login eine unterschiedliche
> Identität an den Service liefern
> und dieser muss das entsprechend handhaben,
> wenn er NFDI Infra Proxy verwenden möchte.

Im Prinzip schon, aber "unterschiedlich" ist etwas zu allgemein. Die
Identität wird mit denselben Attributen beschrieben. Die Werte der
Attribute können unterschiedlich sein.

Insbesondere die verschiedenen Identifier des Nutzers sind hier interessant. Hier wird es kompliziert und ich muss auf die Doku verweisen,
die ich gerade schreibe.

Es ist möglich, unterschiedliche Identitäten eines Nutzers
zusammenzuführen. Dies sollte am Infrastructure Proxy geschehen, kann aber
auch am Dienst gemacht werden.

Gründe für unterschiedliche Identitäten:

- Nutzer kommt über verschiedene Community AAIs zum selben Dienst.
- Nutzer verwendet unterschiedliche Home-IdPs (z.B. Uni und Google)

> Werden zwischen den vier Community AAIs Informationen ausgetauscht?

Nein ein Austausch auf dieser Ebene ist nicht vorgesehen. Daher müssen
sich Community AAIs für eine Lösung entscheiden und sich vollständig dort
organisieren.

> Kann die "NFDI AA" kann Attribute aus mehreren Community AAIs abfragen?
> Was hat es mit dem Thema
> Selbstverwaltung auf sich.

Hier ist der Gedanke, dass die NFDI zentral (also in dieser NFDI-AA) angeben
kann, wer z.B. autorisiert ist, in einer Community AAI Admin zu sein. Dies
kann sich durchaus noch ändern und wird Anfangs manuell gehandhabt.

## Organisatorisches

> Von wem wird die Entscheidung für eine Software-Lösung für die Community AAI
> getroffen? (Als Dienst eine passende Anbindung anhand notwendiger Attribute
> oder Verfahren zu wählen ist verständlich. Aber Dienste entscheiden ja nicht
> für ihre Community oder ihr Konsortium, was eine passende Community AAI wäre
> oder welche VOs benötigt werden, wenngleich sie das mitgestalten müssten.)

Eine Community stellen wir uns als gut organisierte Einheit vor.
Typischerweise ist das ein NFDI Konsortium.

> Wird z.B. aus einem Konsortium oder einer
> Sammlung von Diensten ein Administrator definiert wird, der dann die
> Entscheidung für eine (bestehende/laufende) Community AAI trifft und dort
> VOs und Attribute pflegt.

Ja. Das Konsortium sagt: Wir verwenden Community AAI X. Dienste die nicht
an diese angebunden sind (direkt oder über den Infrastructure Proxy),
gehören nicht zur Community.

> Oder ist der Gedanke dahinter, dass es von den vier Arten von Community AAIs
> jeweils mehrere, durch verschiedene Communities gepflegte Instanzen gibt?
> Also würde man aus NFDI4Chem mehrere gleiche oder verschiedene
> Community AAIs anhand der angebotenen Lösungen aufbauen?

Nein, auf keinen Fall. NFDI4Chem wäre eine Community AAI, die sich für eine
Lösung entscheiden müssen. Dann Pflegen die ihre Dienste alle darin.

## Dienstanbindung

> Als Dienst (z.B. Repository oder ELN) binde ich den NFDI Infra Proxy per
> SAML oder OpenID Connect ein und habe damit alle Nutzer aus DFN, eduGAIN
> und ORCID drin.

Exakt. Die Dienste werden in der Lage sein, die unterschiedliche Qualität
der Identitäten (Google vs. DFN-AAI, z.B. wurde ein Ausweis geprüft) auf
einfache Art zu prüfen.

> Der Nutzer würde, je nach Heimatorganisation, eine der vier wählen und ich habe
> eine oder ggf. später mehrere verknüpfte IdPs zu dieser Identität und
> kann Attribute anfordern.

Nein. Dienste fordern die Attribute nicht direkt bei den IdPs an, sonder
erhalten diese ausschließlich(*) über den Proxy an den sie angebunden
sind.

(*) Diensten steht es natürlich frei, zusätzliche Attribute einzusammeln.
Das ist (zumindest am Anfang) nicht in der Architektur vorgesehen, und
kann vorerst nicht unterstützt werden.

> An welcher Stelle würde man eine virtuelle Organisation pflegen, die den
> Zugang zu ELN/Repo regelt?

Die VOs werden in der jeweiligen Community AAI gepflegt. Das ist deren
Hauptzweck.

> Kann ein Dienst Attribut von einer Heimatorganisation verwenden?

Es ist vorgesehen, ausgewählte Attribute von der Heimatorganisation an den
Dienst weiterzuleiten. Dies soll dem Dienst ermöglichen, beispielsweise
den Status eines Nutzers (Student, Staff, ...) oder Berechtigungen über
"Entitlements" zu nutzen.
Auch die "Assurance" wird von der Heimatorganisation weitergeleitet
(sofern diese die liefert).

> Rechte im Hinblick auf Rollen/Gruppen oder Nutzer zu bestimmten Datensätzen
> muss der Service in Ergänzung zu bestehenden Attributen pflegen, oder pflegt
> man dies aus den vier Community AAIs heraus?

Aus diesem Grund gibt es unterschiedliche Community AAIs.
Die Art und weise wie z.B. Gruppen,
Rollen, Datensatzzugriffsrechte oder Dinge wie Embargos gepflegt werden,
bzw. ob es solche features überhaupt gibt unterscheidet sich sicherlich.

> In Summe ist ein "NFDI Login" damit keine eigene "NFDI Login"-Community AAI
> sondern es werden die bestehenden Community AAIs genutzt und ein Proxy
> angeboten für Dienste, die mehrere Community AAIs benötigen, oder?

Richtig. Damit fügen sich die NFDI AAIs in das größere Bild von EOSC ein.
Dies wird eine einfache Zusammenarbeit mit ähnlichen Lösungen
(LifeScience-RI (vormals ELIXIR-AAI), EGI, EUDAT, HIFIS, ...) ermöglichen:

- Deren Infrastrukturen werden entweder über deren Infrastructure Proxy
    zugänglich gemacht.
- Dienste, die deren Community Attribute einsetzen möchten können sich an
    deren Community AAI (entweder direkt oder via Infrastructure Proxy)
    anbinden.

> Als Service müsste ich nur den einen Proxy oder die eine AAI bedenken und
> das IDM-Team meiner Heimatorganisation muss eigentlich auch nur aktiv werden, wenn
> ich als Service direkt komme, und nicht über den NFDI Proxy oder eine der
> vier Community AAIs, oder?

Exakt. Und dazu sei noch gesagt, dass es jedem Service frei gestellt ist,
wo genau er sich andockt (Heimatorganisation, Community AAI, Infrastructure Proxy).

> Ist die AAI nur mit den vier genannten Community AAI Produkten
> kompatibel, oder gibt es auch die Möglickeit, andere einzusetzen?

Die NFDI AAI ist grundsätzlich so aufgebaut, dass eine möglichst große
Anzahl von unterschiedlichen Komponenten eingesetzt werden kann. Dies
trifft insbesondere auf die Community AAI Produkte zu. Sofern die
Attribute und Policies den AEGIS-Endorsed Recommendations entsprechen,
können diese als Community AAIs eingebunden werden. Eine Liste der
wachsenden Produkte können wir an dieser Stelle nicht pflegen, zumal
manche Produkte unterschiedliche Attribut-Profile unterstützen. Hiervon
hängt die Kompoatibilität entscheidend ab.


> Da alle vier AAI-Lösungen auf der AARC Blueprint Architecture basieren,
> scheint mir, dass alle gleiche bzw. vergleichbare Funktionen bieten.
> Welche Unterschiede gibt es zwischen den vier AAIs? D.h. womit könnte
> man die Entscheidung für eine der Lösungen begründen? Auf dem IAM4NFDI
> Website habe ich zwar die Feature Matrix zum Vergleich der Community
> AAIs gefunden, aber das hilft uns nur bedingt bei unserer Entscheidung. 

So eine Vergleichstabelle ist leider ziemlich politisch, und auch viel
Arbeit die zudem noch ständiger Veränderung unterliegt.

Deshalb hatten wir angefangen, diese Feature Tabelle zu machen, in der die
Community AAI Lösungen selbst eintragen, welche Features sie kennzeichnen.
Leider haben alle von uns zu viel um die Ohren, so dass wir letztlich mit
den Demo Instanzen eine Ergänzung zur Tabelle aufgebaut haben.

Letztlich können die Konsortien dort auch "No preference" angeben. Dann
überlegen wir uns, welche CAAI Lösung am besten zum jeweiligen NFDI
Konsortium passt. Solange es nicht das eine besondere Feature an einer der
Lösungen ist, dass Euer Konsortium benötigt, wird das für Euch vermutlich
keinen wesentlichen Unterschied bedeuten.
