![logo](images/base4NFDI_kurz_rgb.png){: style="width:200px"}

# NFDI AAI Documentation

!!! News
    23-08-15: Proposal for funding an NFDI IAM infrastructure (**Integration** Phase)
    submitted. [Find the full proposal
    here](documents/iam4nfdi_integration.pdf).

    23-02-14: Proposal for funding an NFDI IAM infrastructure (**Initialisation** Phase)
    submitted. [Find the full proposal
    here](documents/iam4nfdi_initialization.pdf).

## Context

The NFDI AAI (architecture, policies, attributes, etc.) described in this
context, is based on the work, the experience, and standardisation made in
previous projects. In the European context these are most notably
[AARC](https://aarc-community.org) and the [EOSC-Taskforce on AAI
Architecture](https://www.eosc.eu/advisory-groups/aai-architecture).

In the German context, several different projects have contributed their
experience to this AAI:

- [Helmholtz AAI](https://hifis.net/aai) by [HIFIS](https://hifis.net),
    which uses Unity in an AARC / EOSC compatible configuration.
- [AcademicID](https://academy.gwdg.de) by [GWDG](https://gwdg.de), which
    is used to offer all IT services of GWDG to education in Lower
    Saxony.
- [didmos](https://daasi.de/en/federated-identity-and-access-management/iam-solutions/didmos/)
    by [DAASI International](https://daasi.de), which integrates
    expandable open source modules that can be custom tailored to
    infrastructures
- [RegApp](https://www.scc.kit.edu/dienste/regapp.php) by
    [KIT](https://kit.edu), which is used to provide access to
    several educational and infrastructure services to academia in Baden
    Württemberg.

## The  Target Audience

This documentation is targeted at **system administrators and technical
people** working on the integration of services with the federated IAM / AAI
infrastructure.

There will be documentation for end users, once the initial set of
services in integrated, which may be used to guide users. It is expected
that login will not be more complex than a standard web login.

## Mailinglist

To stay connected and be informed about updates and events, you should
[subscribe to the NFDI-AAI-INFO mailinglist here](https://www.lists.kit.edu/sympa/subscribe/nfdi-aai-info?previous_action=info).

## Goals

NFDI AAI Architecture addresses these key goals:

1. Architecture that provides a solution for organisation of individual
   NFDI consortia and the whole of NFDI

1. Ensure compatibility with external / international partners
    - Follow the AARC Blueprint Architecture
    - Be compatible with the EOSC AAI

1. Provide an initial Proof of Concept installation

## How can NFDI Consortia join?

NFDI Consortia (=Communities) will need to use a so called
["Community AAI"](community-aai-software.md) service at which they can organise themselves (i.e. they
manage their Virtual Organisation Membership and the subgroups in them).
Therefore, the four software products, which are available in this context in Germany, are
suggested for use within the NFDI context:
AcademicID, didmos, RegAPP, and Unity (HIFIS).

!!! info 
    Community and Virtual Organisation (VO) are often used synonymously.
    Generally, here we understand an NFDI Consortium to be a Community. 
    This community uses the "Community-AAI" to represent its internal
    structure in the form of VOs and sub-VOs.

## How can Services be connected?

For details, please see the [architecture](architecture.md) page
Services may be integrated at three levels, depending on their needs:

- Home-IdPs (via eduGAIN): but then (of course) no community attributes
  (ID, VO-Membership is available)
- Community AAIs: For services that only need to be accessible for a
  single community / NFDI
- Infrastructure Proxy: For services that want to be accessible for more
  than a single community / NFDI.

The Infrastructure Proxy is foreseen to be available as a development
version soon. It will be extended along the beginning of Base4NFDI/IAM.

Before the funding situation of the Base4NFDI/IAM consortium is clarified,
it is not possible to make any statement regarding the responsibility for
the operation of the Community AAIs.
