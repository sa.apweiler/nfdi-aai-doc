![logo](images/base4NFDI_kurz_rgb.png){: style="width:200px"}

# Policies and Templates


## NFDI Policies

<small>( Current version: 0.9.1 )</small>

These Policies are defined by NFDI. All members must follow these
policies:

- Top Level Policy: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/00_Top-Level-Policy.pdf?job=build-docs)
- VO Membership Management Policy (VOMMP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/01_CAAI-VOMMP.pdf?job=build-docs)
- Security Incident Response Proecedure (SIRP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/02_SIRP.pdf?job=build-docs)
- Policy on Processing of Personal Data (PPPD): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/03_PPPD.pdf?job=build-docs)
- Infrastructure Attribute Policy (IAP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/04_IAP.pdf?job=build-docs)
- Community AAI Acceptable Use Policy (CAAI-AUP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/05-CAAI_AUP.pdf?job=build-docs)


## NFDI Policy Templates

<small>( Current version: 0.9.1 )</small>

These templates are defined by NFDI. They MAY be used as a template for
defining own policies. (In general these templates make it safer to follow
existing regulations than self-written policies.)

The documents can be edited with Libreoffice or compatible Software.

- VO Acceptable Use Policy (VO-AUP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/05-VO_AUP_template.pdf?job=build-docs) [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/05-VO_AUP_template.docx?job=build-docs)
- Service Access Policy (SAP): [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/06_SAP_template.pdf?job=build-docs) [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/06_SAP_template.docx?job=build-docs)
- VO Lifecycle Policy: [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/91_VO-lifecycle.pdf?job=build-docs) [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/91_VO-lifecycle.docx?job=build-docs)
- Pricay Statement Template [pdf](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/07_Privacy_Policy_Template.pdf?job=build-docs) [docx](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/07_Privacy_Policy_Template.docx?job=build-docs)
- Template Verfahrensverzeichnis: [odt](https://codebase.helmholtz.cloud/m-team/nfdi/nfdi-policies/-/jobs/artifacts/v0.9.1/raw/Template_Verfahrensverzeichnis_AAI.odt?job=build-docs)


## Underlying Policy Frameworks

The following Policy Frameworks / Recommendations are intended to be supported:

- [Security Incident Response Trust Framework for Federated Identity (Sirtfi)](https://refeds.org/sirtfi)
- [AARC Guidelines Documents Approved by AEGIS](https://wiki.geant.org/display/AARC/AARC+Documents+Approved+by+AEGIS)
- [REFEDS Data Protection Code of Conduct 2.0 (DPCoCo2)](https://refeds.org/category/code-of-conduct)

